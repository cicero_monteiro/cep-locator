package com.cicero.demo.db;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.cicero.demo.domain.Address;

public interface AddressRepository extends MongoRepository<Address, Long> {
    
    Address findById(String id);

}
