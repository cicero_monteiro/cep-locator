package com.cicero.demo.service;

import java.util.List;

import com.cicero.demo.domain.Customer;

public interface CustomerService {
    Customer findByFirstName(String firstName);

    List<Customer> findByLastName(String lastName);

    Customer save(Customer customer);
}
