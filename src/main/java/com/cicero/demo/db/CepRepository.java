package com.cicero.demo.db;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.cicero.demo.domain.CEP;

public interface CepRepository extends MongoRepository<CEP, Long> {
    public CEP findByCep(Long cep);
}
